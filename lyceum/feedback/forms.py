from django import forms

from feedback.models import CustomerFeedback, Feedback


__all__ = []


class PersonalForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.visible_fields():
            field.field.widget.attrs["class"] = "form-control"

    class Meta:
        model = CustomerFeedback

        fields = "__all__"

        labels = {
            CustomerFeedback.name.field.name: "Имя",
            CustomerFeedback.mail.field.name: "Почта",
        }

        help_texts = {
            CustomerFeedback.mail.field.name: "Введите почтовый адрес",
        }


class FeedbackForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.visible_fields():
            field.field.widget.attrs["class"] = "form-control"

    class Meta:
        model = Feedback
        exclude = (
            Feedback.created_on.field.name,
            Feedback.status.field.name,
            Feedback.customer.field.name,
        )
        labels = {
            Feedback.text.field.name: "Текст",
        }

        help_texts = {
            Feedback.text.field.name: "Напишите отзыв",
        }


class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True
    is_required = False


class MultipleFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("widget", MultipleFileInput())
        super().__init__(*args, **kwargs)
        self.widget.attrs["class"] = "form-control"
        self.required = False

    def clean(self, data, initial=None):
        single_file_clean = super().clean
        if isinstance(data, (list, tuple)):
            return [single_file_clean(d, initial) for d in data]
        return single_file_clean(data, initial)


class FileFieldForm(forms.Form):
    file = MultipleFileField()
