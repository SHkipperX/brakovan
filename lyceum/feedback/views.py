from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.shortcuts import redirect, render
from django.urls import reverse

from feedback.forms import FeedbackForm, FileFieldForm, PersonalForm
from feedback.models import File

__all__ = []

PATH = "feedback/"


def feedback(request):
    temaplate = PATH + "feedback.html"
    context = {}
    url = reverse("feedback:feedback")

    form_feedback = FeedbackForm(request.POST or None)
    form_personal = PersonalForm(request.POST or None)
    form_files = FileFieldForm(request.POST or None)
    if False not in (
        form_feedback.is_valid(),
        form_personal.is_valid(),
        form_files.is_valid(),
        request.method == "POST",
    ):
        messages.success(request, "Форма успешно отправлена!")
        text = form_feedback.cleaned_data.get("text")
        mail = form_personal.cleaned_data.get("mail")
        send_mail(
            subject="Subject",
            message=text,
            from_email=settings.DJANGO_MAIL,
            recipient_list=[mail],
            fail_silently=False,
        )
        form_personal.full_clean()
        save_personal = form_personal.save()
        form_feedback.full_clean()
        pre_save_feedback_form = form_feedback.save(commit=False)
        pre_save_feedback_form.customer = save_personal
        pre_save_feedback_form.save()
        for i in request.FILES.getlist("file"):
            file = File(
                feedback=pre_save_feedback_form,
                file=i,
            )
            file.save()

        return redirect("about:thanks_for_feedback")

    context["url"] = url
    context["form_personal"] = form_personal
    context["form_feedback"] = form_feedback
    context["form_file"] = form_files
    return render(request, temaplate, context)
