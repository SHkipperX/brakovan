from django.contrib import admin

from feedback.models import CustomerFeedback, Feedback, StatusLog

__all__ = []


class FeedbacksInline(admin.TabularInline):
    model = Feedback
    readonly_fields = (
        Feedback.text.field.name,
        Feedback.created_on.field.name,
    )
    can_delete = False
    extra = 1


@admin.register(CustomerFeedback)
class CustomerFeedbackadmin(admin.ModelAdmin):
    inlines = [FeedbacksInline]
    readonly_fields = (
        CustomerFeedback.name.field.name,
        CustomerFeedback.mail.field.name,
    )

    def save_model(self, request, obj, form, change):
        feedback = Feedback.objects.get(customer=obj)
        status_log = StatusLog.objects.filter(feedback=feedback.pk).first()
        updated_field = request.POST.get("feedback_set-__prefix__-status")
        if change and feedback.status != updated_field:
            sts = {
                "from": feedback.status,
            }
            if not status_log:
                StatusLog.objects.create(
                    user=request.user,
                    feedback_id=feedback.pk,
                    **sts,
                    to=updated_field,
                )
            else:
                status_log.from_status = feedback.status
                status_log.to_status = updated_field
                status_log.full_clean()
                status_log.save()
        return super().save_model(request, obj, form, change)
