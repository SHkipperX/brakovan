from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, override_settings, TestCase
from django.urls import reverse

from feedback.forms import FeedbackForm, FileFieldForm, PersonalForm
from feedback.models import CustomerFeedback, Feedback, File

__all__ = []


class TestForm(TestCase):
    class Fields:
        text_valid = "прочтили - балл подкиньте)"
        text_invalid = ""

        mail_valid = "www.django@pepe.ru"
        mail_invalid = "www.|.www"

        file = SimpleUploadedFile(
            "test.txt",
            b"KPoKoDuJl_Da",
        )

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.feedback = FeedbackForm
        cls.file = FileFieldForm
        cls.personal = PersonalForm

    def tearDown(self):
        CustomerFeedback.objects.all().delete()
        Feedback.objects.all().delete()
        File.objects.all().delete()
        return super().tearDown()

    @override_settings(MEDIA_ROOT="media/test_media/")
    def test_forms_valid(self):
        data = {
            "mail": self.Fields.mail_valid,
            "text": self.Fields.text_valid,
            "file": [self.Fields.file],
        }
        upload = File.objects.all().count()
        count = Feedback.objects.all().count()
        response = Client().post(
            reverse("feedback:feedback"),
            data=data,
        )
        person = CustomerFeedback.objects.first()
        upload_related = Feedback.objects.get(customer=person)
        self.assertIsNotNone(upload_related)
        self.assertEquals(response.status_code, 302)
        self.assertEquals(Feedback.objects.all().count(), count + 1)
        self.assertEquals(File.objects.all().count(), upload + 1)

    def test_redirect(self):
        data = {
            "mail": self.Fields.mail_valid,
            "text": self.Fields.text_valid,
            "file": [
                SimpleUploadedFile(
                    "test2.txt",
                    b"KPoKoDuJl_Da",
                    "aplication/txt",
                ),
            ],
        }
        response = Client().post(
            reverse("feedback:feedback"),
            data=data,
            follow=True,
        )
        self.assertRedirects(response, reverse("about:thanks_for_feedback"))

    def test_invalid_forms(self):
        data = {
            "mail": self.Fields.mail_invalid,
            "text": self.Fields.text_invalid,
        }
        response = Client().post(
            reverse("feedback:feedback"),
            data=data,
        )

        self.assertTrue(response.context["form_feedback"].has_error("text"))
        self.assertTrue(response.context["form_personal"].has_error("mail"))

    def test_check_feedback(self):
        feedback = self.feedback(self.Fields.text_valid).base_fields["text"]
        self.assertEquals(feedback.label, "Текст")
        self.assertEquals(feedback.help_text, "Напишите отзыв")

    def test_check_personal(self):
        feedback = self.personal(self.Fields.mail_valid).base_fields["mail"]
        self.assertEquals(feedback.label, "Почта")
        self.assertEquals(feedback.help_text, "Введите почтовый адрес")
