from django.conf import settings
from django.db import models

__all__ = []


STATUS = (
    ("GET", "получено"),
    ("REC", "в обработке"),
    ("POST", "ответ дан"),
)


class CustomerFeedback(models.Model):
    name = models.CharField(
        verbose_name="имя",
        max_length=50,
        null=True,
        blank=True,
    )

    mail = models.EmailField(
        verbose_name="почта",
    )

    class Meta:
        verbose_name = "персональные данные"
        verbose_name_plural = "персональные данные"

    def __str__(self) -> str:
        return self.mail


class Feedback(models.Model):
    customer = models.OneToOneField(
        CustomerFeedback,
        on_delete=models.CASCADE,
    )

    text = models.TextField(
        verbose_name="текст",
    )
    created_on = models.DateTimeField(
        verbose_name="время создания",
        auto_now_add=True,
    )

    status = models.CharField(
        verbose_name="статус",
        choices=STATUS,
        max_length=4,
        default="GET",
    )

    class Meta:
        verbose_name = "фидбэк"
        verbose_name_plural = "фидбэки"


class File(models.Model):
    def upload_to(self, filename):
        return f"uploads/{self.feedback_id}/{filename}"

    feedback = models.OneToOneField(
        Feedback,
        on_delete=models.CASCADE,
    )
    file = models.FileField(
        upload_to=upload_to,
    )

    def __str__(self):
        return self.file.name


class StatusLog(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name="пользователь",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    feedback = models.OneToOneField(
        Feedback,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        unique=False,
    )
    timestamp = models.DateTimeField(
        auto_now_add=True,
    )
    from_status = models.CharField(
        verbose_name="исходный статус",
        choices=STATUS,
        max_length=4,
        db_column="from",
        name="from",
    )
    to = models.CharField(
        verbose_name="конечный статус",
        choices=STATUS,
        max_length=4,
        db_column="to",
        name="to",
    )

    class Meta:
        verbose_name = "статус обращения"
        verbose_name_plural = "статус обращений"

    def __str__(self):
        return self.feedback.mail
