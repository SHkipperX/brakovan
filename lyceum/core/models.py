from django.core import validators
from django.db import models

__all__ = []


class AbsNamePublishesed(models.Model):
    name = models.CharField(
        verbose_name="название",
        max_length=150,
        validators=[validators.MaxLengthValidator(150)],
        unique=True,
        help_text="Any",
    )

    is_published = models.BooleanField(
        verbose_name="опубликовано",
        default=True,
    )

    class Meta:
        abstract = True
