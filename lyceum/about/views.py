from django.shortcuts import render
from django.urls import reverse

__all__ = []

PATH = "about/"


def description(request):
    template = PATH + "about.html"

    url = reverse("about:about")
    context = {"url": url}

    return render(request, template, context)


def redirect_feedback(request):
    template = PATH + "thx_feedback.html"
    url = reverse("about:thanks_for_feedback")
    context = {"url": url}

    return render(request, template, context)
