from http import HTTPStatus

from django.test import Client, TestCase
from django.urls import reverse

__all__ = []


class StaticUrlTest(TestCase):
    def test_about_endpoint(self):
        response = Client().get(reverse("about:about"))
        self.assertContains(response, "О проекте", status_code=HTTPStatus.OK)
