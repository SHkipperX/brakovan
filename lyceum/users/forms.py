from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.models import User

from users.models import Profile

__all__ = []


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = (
            "username",
            "email",
            "password1",
            "password2",
        )
        widgets = {
            "email": forms.EmailInput(
                attrs={
                    "required": True,
                },
            ),
        }


class UserForm(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        for field in self.visible_fields():
            field.field.widget.attrs["class"] = "form-control"

    password = None

    class Meta(UserChangeForm.Meta):
        model = User
        fields = (
            User.first_name.field.name,
            User.last_name.field.name,
            User.email.field.name,
        )
        exclude = (User.password.field.name,)


class ProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        for field in self.visible_fields():
            field.field.widget.attrs["class"] = "form-control"

    class Meta:
        model = Profile
        fields = (
            Profile.birthday.field.name,
            Profile.image.field.name,
            Profile.coffee_count.field.name,
        )

        widgets = {
            Profile.coffee_count.field.name: forms.NumberInput(
                attrs={
                    "disabled": True,
                },
            ),
        }


class EditProfile(forms.Form):
    profile = ProfileForm()
    user = UserForm()


class CoffeeBlob(forms.Form):
    pass
