from unittest.mock import patch

from django.contrib.auth.models import User
from django.test import Client, override_settings, TestCase
from django.urls import reverse
from django.utils import timezone

__all__ = []


class ActivationTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = User.objects.create_user(
            username="Alehandros",
            password="PepeTheFrog",
            is_active=False,
        )
        cls.Url = reverse(
            "users:activate",
            args=[cls.user.username],
        )
        cls.Url2 = reverse(
            "users:activate",
            args=["Danila"],
        )
        User.objects.create_user(
            username="Apostol",
            password="Petr228Xx",
            is_active=True,
            email="pepe@mail.ru",
        )

    def tearDown(self) -> None:
        User.objects.all().delete()
        return super().tearDown()

    @patch("users.views.timezone.now")
    def test_not_activate_user(self, mock_change_time):
        mock_change_time.return_value = (
            self.user.date_joined + timezone.timedelta(hours=11)
        )
        response = Client().get(self.Url)
        self.assertContains(
            response,
            (
                "Аккаунт уже активирован или не прошло"
                " 12 часов с момента создания аккаунта!"
            ),
        )
        self.assertFalse(
            User.objects.get(username=self.user.username).is_active,
        )

    @patch("users.views.timezone.now")
    def test_activate_user(self, mock_change_time):
        mock_change_time.return_value = (
            self.user.date_joined + timezone.timedelta(hours=12, minutes=1)
        )
        response = Client().get(self.Url)
        self.assertContains(
            response,
            "Аккаунт активирован!",
        )
        self.assertTrue(
            User.objects.get(username=self.user.username).is_active,
        )

    @override_settings(DEFAULT_USER_IS_ACTIVE=False)
    def test_register_user_to_activate_user(self):
        data = {
            "username": "Danila",
            "email": "Ratatui@google.ya",
            "password1": "PlovIzDjangistov",
            "password2": "PlovIzDjangistov",
        }

        response = Client().post(
            reverse("users:signup"),
            data=data,
            follow=True,
        )
        user = User.objects.get(
            username=data["username"],
        )
        self.assertContains(
            response,
            "Профиль успешно сохранён! Теперь подтвердите его!",
        )

        with patch("users.views.timezone.now") as mock_change_time:
            mock_change_time.return_value = (
                user.date_joined + timezone.timedelta(hours=11)
            )
            response = Client().get(self.Url2)

            user = User.objects.get(username=data["username"])
            self.assertFalse(user.is_active)

            mock_change_time.return_value += timezone.timedelta(hours=2)
            response = Client().get(self.Url2)
            user = User.objects.get(username=data["username"])
            self.assertTrue(user.is_active)

    def test_login_username(self):
        response = self.client.post(
            reverse("users:login"),
            data={
                "username": "Apostol",
                "password": "Petr228Xx",
            },
            follow=True,
        )
        self.assertRedirects(response, reverse("homepage:main"))
        self.assertEquals(response.context["user"].username, "Apostol")

    def test_login_email(self):
        response = self.client.post(
            reverse("users:login"),
            data={
                "username": "pepe@mail.ru",
                "password": "Petr228Xx",
            },
            follow=True,
        )

        self.assertRedirects(response, reverse("homepage:main"))
        self.assertEquals(response.context["user"].username, "Apostol")
