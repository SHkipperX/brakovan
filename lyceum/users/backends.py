from django.contrib.auth import get_user_model
from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.hashers import check_password

__all__ = []


class LoginUsernameEmail(BaseBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        get_user = None
        if password and username:
            try:
                get_user = get_user_model().objects.get(username=username)
            except get_user_model().DoesNotExist:
                get_user = get_user_model().objects.get(email=username)

            pwd_valid = check_password(password, get_user.password)
            if pwd_valid and get_user:
                return get_user

        return None

    def get_user(self, username):
        try:
            return get_user_model().objects.get(pk=username)
        except get_user_model().DoesNotExist:
            return None
