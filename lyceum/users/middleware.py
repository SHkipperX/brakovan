from users.models import User

__all__ = []


class MiddlewareAuth:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.id:
            request.user = User.objects.load_profile_data(request.user)
        return self.get_response(request)
