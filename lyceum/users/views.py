from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.shortcuts import get_list_or_404, redirect, render
from django.urls import reverse
from django.utils import timezone


from users.forms import EditProfile, ProfileForm, RegisterForm, UserForm
from users.models import Profile, User

PATH = "users/"

__all__ = []


def register(request):
    context = {}
    template = PATH + "signup.html"
    form = RegisterForm(request.POST or None)
    context["form"] = form
    if False not in (form.is_valid(), request.method == "POST"):
        formclean = form.cleaned_data
        username = formclean.get("username")
        email = formclean.get("email")
        if User.objects.filter(username=username).first():
            context["form"].add_error(
                "username",
                "Пользователь с таким именем уже существует!",
            )

        if User.objects.filter(email=email).first():
            context["form"].add_error(
                "email",
                "Пользователь с такой почтой уже существует!",
            )
        if context["form"].has_error("email") or context["form"].has_error(
            "username",
        ):
            return render(request, template, context)
        user = form.save(commit=False)
        user.is_active = settings.DEFAULT_USER_IS_ACTIVE
        user.save()
        Profile.objects.create(user=user)
        messages.success(
            request,
            "Профиль успешно сохранён! Теперь подтвердите его!",
        )

        send_mail(
            "Активация",
            f"http://{request.get_host()}/auth/activate/{username}/",
            from_email=settings.DJANGO_MAIL,
            recipient_list=[email],
            fail_silently=False,
        )
        context["form"] = RegisterForm(None)
        return render(request, template, context)
    return render(request, template, context)


def activate(request, username):
    template = PATH + "activate_user.html"
    context = {}
    user = User.objects.get(username=username)
    date_activate = user.date_joined + timezone.timedelta(hours=12)
    if (
        date_activate.timestamp() <= timezone.now().timestamp()
    ) and not user.is_active:
        user.is_active = True
        user.save()
        context["status"] = "Аккаунт активирован!"
        context["class"] = "bg-success"
    else:
        context["status"] = (
            "Аккаунт уже активирован или не прошло"
            " 12 часов с момента создания аккаунта!"
        )
        context["class"] = "bg-danger"
    return render(request, template, context)


def user_list(request):
    template = PATH + "user_list.html"
    context = {}

    users = get_list_or_404(User.objects.active())
    context["users"] = users
    return render(request, template, context)


def user_detail(request, user_id):
    template = PATH + "user_detail.html"
    context = {}
    if request.user.id == user_id:
        return redirect(reverse("users:profile"))
    user = User.objects.filter(id=user_id).first()
    context["user"] = user
    return render(request, template, context)


@login_required
def profile(request):
    template = PATH + "profile.html"
    context = {}
    form_user = UserForm(
        request.POST or None,
        instance=request.user,
    )
    form_profile = ProfileForm(
        request.POST or None,
        request.FILES or None,
        instance=request.user.profile,
    )

    if False not in (
        form_user.is_valid(),
        form_profile.is_valid(),
        "change" in request.POST,
        request.method == "POST",
    ):
        form_user.save()
        form_profile.save()

    form = EditProfile()
    form.user = form_user
    form.profile = form_profile

    context["user"] = request.user
    context["form"] = form
    return render(request, template, context)
