from django.contrib.auth.models import User as DjangoUser
from django.db import models


__all__ = []


class Profile(models.Model):
    def upload_to(self, filename):
        return f"avatars/{self.pk}_{filename}"

    user = models.OneToOneField(
        DjangoUser,
        on_delete=models.CASCADE,
    )

    birthday = models.DateField(
        verbose_name="день рождение",
        help_text="формата: день:месяц:год",
        blank=True,
        null=True,
    )
    image = models.ImageField(
        upload_to=upload_to,
        verbose_name="аватар",
        blank=True,
        null=True,
    )

    coffee_count = models.PositiveIntegerField(
        verbose_name="кофе сварено",
        default=0,
    )

    class Meta:
        verbose_name = "профиль"
        verbose_name_plural = "профили"


class DjangoUserManager(models.Manager):
    use_in_migrations = True
    username = DjangoUser.username.field.name
    first_name = DjangoUser.first_name.field.name
    last_name = DjangoUser.last_name.field.name
    email = DjangoUser.email.field.name
    date_joined = DjangoUser.date_joined.field.name
    last_login = DjangoUser.last_login.field.name
    is_superuser = DjangoUser.is_superuser.field.name
    is_staff = DjangoUser.is_staff.field.name
    is_active = DjangoUser.is_active.field.name

    birthday = "profile__birthday"
    image = "profile__image"
    coffee_count = "profile__coffee_count"

    def load_profile_data(self, user):
        return super().get_queryset().select_related("profile").get(id=user.pk)

    def active(self):
        return super().select_related("profile").filter(is_active=True)

    def by_mail(self, email):
        return super().get(email=email)

    def full_user_data(self, user):
        return super().select_related("profile").get(id=user.pk)


class User(DjangoUser):
    objects = DjangoUserManager()

    DjangoUser._meta.get_field("email")._unique = True

    class Meta:
        proxy = True
        managed = False
