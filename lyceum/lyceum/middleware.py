import re

from django.conf import settings

__all__ = []


class CustomMiddleware:
    counter = 0

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        self.update_counter()
        if self.counter % 10 == 0 and settings.ALLOW_REVERSE:
            content = response.content.decode()
            response.content = self.reverse(content)

        return response

    @classmethod
    def update_counter(cls):
        cls.counter += 1

    def reverse(self, content):
        pattern = r"\b[а-яА-ЯёЁ]+\b"
        return re.sub(pattern, lambda match: match.group()[::-1], content)
