from django.test import Client, TestCase
from django.test.utils import override_settings
from django.urls import reverse

__all__ = []


class MiddlewareTests(TestCase):
    @override_settings(ALLOW_REVERSE=True)
    def test_custommiddleware_enabled(self):
        for i in range(1, 21):
            content = Client().get(reverse("homepage:coffee")).content
            if i % 10 == 0:
                self.assertEqual("Я кинйач".encode(), content)
            else:
                self.assertEqual("Я чайник".encode(), content)

    @override_settings(ALLOW_REVERSE=False)
    def test_custommiddleware_disabled(self):
        for i in range(1, 21):
            content = Client().get("/coffee/").content
            self.assertEqual("Я чайник".encode(), content)
