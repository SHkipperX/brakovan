from pathlib import Path

from decouple import config
from django.utils.translation import gettext_lazy as _


BASE_DIR = Path(__file__).resolve().parent.parent

config.search_path = BASE_DIR / ".env"

TRUE_VALUES = ("true", "yes", "y", "t", "1")

SECRET_KEY = config("DJANGO_SECRET_KEY", default="Polsha")

DEBUG = config(
    "DJANGO_DEBUG",
    default="False",
    cast=lambda x: x.lower() in TRUE_VALUES,
)


ALLOW_REVERSE = config(
    "DJANGO_ALLOW_REVERSE",
    default="True",
    cast=lambda x: x.lower() in TRUE_VALUES,
)


ALLOWED_HOSTS = config(
    "DJANGO_ALLOWED_HOSTS",
    default="*",
    cast=lambda x: x.split(","),
)

DJANGO_MAIL = config(
    "DJANGO_MAIL",
    default="webmaster@localhost.tutu",
    cast=lambda mail: mail.lower(),
)

DEFAULT_USER_IS_ACTIVE = config(
    "DJANGO_USER_IS_ACTIVE",
    default="f",
    cast=lambda x: x in TRUE_VALUES,
)

DEFAULT_CHARSET = "utf-8"

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django.contrib.redirects",
    "about.apps.AboutConfig",
    "homepage.apps.HomepageConfig",
    "catalog.apps.CatalogConfig",
    "download.apps.DownloadConfig",
    "feedback.apps.FeedbackConfig",
    "users.apps.UsersConfig",
    "core.apps.CoreConfig",
    "sorl.thumbnail",
    "django_cleanup.apps.CleanupConfig",
    "ckeditor",
    "ckeditor_uploader",
]

SITE_ID = 1

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "users.middleware.MiddlewareAuth",
]


FILE_UPLOAD_HANDLERS = [
    "django.core.files.uploadhandler.MemoryFileUploadHandler",
    "django.core.files.uploadhandler.TemporaryFileUploadHandler",
]

CLEANUP_KEEP_CLEAN = True

if ALLOW_REVERSE:
    MIDDLEWARE.append("lyceum.middleware.CustomMiddleware")


if DEBUG:
    DEFAULT_USER_IS_ACTIVE = True
    INSTALLED_APPS.append("debug_toolbar")
    MIDDLEWARE.append("debug_toolbar.middleware.DebugToolbarMiddleware")

INTERNAL_IPS = config(
    "INTERNAL_IPS",
    default="127.0.0.1,localhost",
    cast=lambda x: x.split(","),
)

MIGRATION_MODULES = {}

ROOT_URLCONF = "lyceum.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "lyceum.wsgi.application"


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    },
}
AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "users.backends.LoginUsernameEmail",
]

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": (
            "django.contrib.auth.password_validation"
            ".UserAttributeSimilarityValidator"
        ),
    },
    {
        "NAME": (
            "django.contrib.auth.password_validation.MinimumLengthValidator"
        ),
    },
    {
        "NAME": (
            "django.contrib.auth.password_validation.CommonPasswordValidator"
        ),
    },
    {
        "NAME": (
            "django.contrib.auth.password_validation.NumericPasswordValidator"
        ),
    },
]


LOGIN_URL = "/auth/login/"
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/auth/login/"


LANGUAGE_CODE = "ru"

LANGUAGES = [
    ("en", _("English")),
    ("ru", _("Russian")),
]

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = [BASE_DIR / "locale"]

STATIC_URL = "static/"

STATIC_ROOT = "static"

STATICFILES_DIRS = [BASE_DIR / "static_dev", BASE_DIR / "media"]

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

THUMBNAIL_DEBUG = True

MEDIA_URL = "/media/"

MEDIA_ROOT = BASE_DIR / "media"


CKEDITOR_JQUERY_URL = (
    "https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"
)
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_IMAGE_BACKEND = "pillow"

CKEDITOR_CONFIGS = {
    "default": {
        "toolbar": "Full",
        "height": 500,
        "width": 900,
        "extraPlugins": "codesnippet",
    },
}

CKEDITOR_UPLOAD_SLUGIFY_FILENAME = False
CKEDITOR_RESTRICT_BY_USER = True
CKEDITOR_BROWSE_SHOW_DIRS = True

EMAIL_BACKEND = "django.core.mail.backends.filebased.EmailBackend"
EMAIL_FILE_PATH = BASE_DIR / "send_mail"
