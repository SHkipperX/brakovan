from django.core import exceptions
from django.test import TestCase


import catalog.models

__all__ = []


class ModelsTest(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.category = catalog.models.Category.objects.create(
            is_published=True,
            name="Тестовая категория",
            slug="test-slug-category",
            weight=500,
        )
        cls.tag = catalog.models.Tag.objects.create(
            is_published=True,
            name="Тестовый тэг",
            slug="test-tag-slug",
        )

    def tearDown(self):
        catalog.models.Tag.objects.all().delete()
        catalog.models.Category.objects.all().delete()
        return super().tearDown()

    def test_itemvalidator_fail(self):
        item_count = catalog.models.Item.objects.count()
        with self.assertRaises(exceptions.ValidationError):
            item = catalog.models.Item(
                is_published=True,
                name="Some_Name",
                category=ModelsTest.category,
                text="Нет роскоши!",
            )
            item.full_clean()
            item.tags.add(ModelsTest.tag)
            item.save()
        self.assertEqual(catalog.models.Item.objects.count(), item_count)

    def test_itemvalidator_success(self):
        item_count = catalog.models.Item.objects.count()
        item = catalog.models.Item(
            is_published=True,
            name="Some_Name",
            text="роскошно",
            category=ModelsTest.category,
        )

        item.full_clean()
        item.save()
        self.assertEqual(catalog.models.Item.objects.count(), item_count + 1)

    def test_itemlenname_fail(self):
        item_count = catalog.models.Item.objects.count()
        with self.assertRaises(exceptions.ValidationError):
            item = catalog.models.Item(
                is_published=True,
                name="a" * 151,
                text="роскошно",
                category=ModelsTest.category,
            )

            item.full_clean()
            item.save()
            item.tags.set([ModelsTest.tag])
        self.assertEqual(catalog.models.Item.objects.count(), item_count)

    def test_tagnamelen_fail(self):
        tag_count = catalog.models.Tag.objects.count()
        with self.assertRaises(exceptions.ValidationError):
            tag = catalog.models.Tag(
                is_published=True,
                name="a" * 151,
                slug="slug",
            )
            tag.full_clean()
            tag.save()

        self.assertEqual(catalog.models.Tag.objects.count(), tag_count)

    def test_tagsluglen_fail(self):
        tag_count = catalog.models.Tag.objects.count()
        with self.assertRaises(exceptions.ValidationError):
            tag = catalog.models.Tag(
                is_published=True,
                name="Some_name",
                slug="a-_" * 70,
            )
            tag.full_clean()
            tag.save()

        self.assertEqual(catalog.models.Tag.objects.count(), tag_count)

    def test_categorynamelen_fail(self):
        category_count = catalog.models.Category.objects.count()
        with self.assertRaises(exceptions.ValidationError):
            tag = catalog.models.Category(
                is_published=True,
                name="a" * 151,
                slug="slug",
            )
            tag.full_clean()
            tag.save()

        self.assertEqual(
            catalog.models.Category.objects.count(),
            category_count,
        )

    def test_categorysluglen_fail(self):
        category_count = catalog.models.Category.objects.count()
        with self.assertRaises(exceptions.ValidationError):
            tag = catalog.models.Category(
                is_published=True,
                name="Some_name",
                slug="a-_" * 70,
            )
            tag.full_clean()
            tag.save()

        self.assertEqual(
            catalog.models.Category.objects.count(),
            category_count,
        )

    def test_categoryweight_fail(self):
        category_count = catalog.models.Category.objects.count()
        for weight_value in [-10, 34000, 1.5, float(0.5)]:
            with self.assertRaises(exceptions.ValidationError):
                tag = catalog.models.Category(
                    is_published=True,
                    name="Some_name",
                    slug="slug",
                    weight=weight_value,
                )
                tag.full_clean()
                tag.save()

        self.assertEqual(
            catalog.models.Category.objects.count(),
            category_count,
        )
