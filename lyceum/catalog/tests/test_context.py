from django.test import Client, TestCase
from django.urls import reverse

import catalog.models

__all__ = []

"""
4 категории:
    2 опубликованы;
    2 неопубликованы;

3 тэга:
    2 опуликованы;
    1 неопубликованы;

9 товара:
    5 опубликованы;
    4 неопубликованы;
"""


class ContextTest(TestCase):
    Items_parametrs = {
        "publick": {
            "catalog": {
                "item_1": {
                    "name": "товар каталога 1",
                    "is_published": True,
                    "is_on_main": False,
                    "text": "роскошно описанный товар",
                },
                "item_2": {
                    "name": "товар каталога 2",
                    "is_published": True,
                    "is_on_main": False,
                    "text": "превосходно написанный текст",
                },
                "item_3": {
                    "name": "товар каталога 3",
                    "is_published": True,
                    "is_on_main": False,
                    "text": "товар не является ххх решение для роскошно",
                },
            },
            "homepage": {
                "item_4": {
                    "name": "товар главной 4",
                    "is_published": True,
                    "is_on_main": True,
                    "text": "гимн твича: *!#$ */-!#% $#*& звучит роскошно",
                },
                "item_5": {
                    "name": "товар главной 5",
                    "is_published": True,
                    "is_on_main": True,
                    "text": (
                        "слона мне! слона! кричал волк."
                        "Превосходно - подумал слон..."
                    ),
                },
                "item_6": {
                    "name": "товар главной 6",
                    "is_published": True,
                    "is_on_main": True,
                    "text": (
                        "роскошно лежал коврик из волка на полу в гостинной"
                    ),
                },
            },
        },
        "unpublick": {
            "item_7": {
                "name": "Неопубликованный товар 7",
                "is_published": False,
                "is_on_main": True,
                "text": "роскошно сидит 7 товар на витрине (слон)",
            },
            "item_8": {
                "name": "Неопубликованный товар 8",
                "is_published": False,
                "is_on_main": False,
                "text": (
                    "Мы не торгуем ничем кроме превосходно собранного хлопка"
                ),
            },
            "item_9": {
                "name": "Неопубликованный товар 9",
                "is_published": False,
                "is_on_main": True,
                "text": "роскошно - подумал штирлиц",
            },
        },
    }

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

        # Опубликованные категории
        cls.category_publick_anans = catalog.models.Category.objects.create(
            is_published=True,
            name="ананас",
            slug="pineapple",
            weight=500,
        )
        cls.category_publick_batat = catalog.models.Category.objects.create(
            is_published=True,
            name="батат",
            slug="batat",
            weight=500,
        )

        # Неопубликованные категории
        cls.category_unpublick_cucumber = (
            catalog.models.Category.objects.create(
                is_published=False,
                name="огурец",
                slug="cucumber",
                weight=5000,
            )
        )
        cls.category_unpublick_ishak = catalog.models.Category.objects.create(
            is_published=False,
            name="ишак",
            slug="ishak",
            weight=700,
        )

        # Опубликованные тэги
        cls.tag_publick_vegetables = catalog.models.Tag.objects.create(
            is_published=True,
            name="вощ",
            slug="vegetables",
        )
        cls.tag_publick_ussr = catalog.models.Tag.objects.create(
            is_published=True,
            name="союз",
            slug="ussr",
        )

        # Неопубликованные тэги
        cls.tag_unpublick_wildeberries = catalog.models.Tag.objects.create(
            is_published=False,
            name="дикие ягоды",
            slug="wildberries",
        )
        cls.tag_unpublick_apple = catalog.models.Tag.objects.create(
            is_published=False,
            name="яблоко",
            slug="apple",
        )

        # Товар размещённый только в каталоге
        cls.item_publick_catalog_1 = catalog.models.Item(
            **cls.Items_parametrs["publick"]["catalog"]["item_1"],
            category=cls.category_publick_batat,
        )
        cls.item_publick_catalog_2 = catalog.models.Item(
            **cls.Items_parametrs["publick"]["catalog"]["item_2"],
            category=cls.category_publick_anans,
        )
        cls.item_publick_catalog_3 = catalog.models.Item(
            **cls.Items_parametrs["publick"]["catalog"]["item_3"],
            category=cls.category_unpublick_cucumber,
        )

        # Товар размещённый на главной и каталоге
        cls.item_publick_all_4 = catalog.models.Item(
            **cls.Items_parametrs["publick"]["homepage"]["item_4"],
            category=cls.category_publick_anans,
        )
        cls.item_publick_all_5 = catalog.models.Item(
            **cls.Items_parametrs["publick"]["homepage"]["item_5"],
            category=cls.category_unpublick_ishak,
        )
        cls.item_publick_all_6 = catalog.models.Item(
            **cls.Items_parametrs["publick"]["homepage"]["item_6"],
            category=cls.category_publick_batat,
        )

        # Скрытый товар
        cls.item_unpublick_7 = catalog.models.Item(
            **cls.Items_parametrs["unpublick"]["item_7"],
            category=cls.category_unpublick_cucumber,
        )
        cls.item_unpublick_8 = catalog.models.Item(
            **cls.Items_parametrs["unpublick"]["item_8"],
            category=cls.category_unpublick_ishak,
        )
        cls.item_unpublick_9 = catalog.models.Item(
            **cls.Items_parametrs["unpublick"]["item_9"],
            category=cls.category_publick_anans,
        )

        for item in [
            cls.item_publick_catalog_1,
            cls.item_publick_catalog_2,
            cls.item_publick_catalog_3,
            cls.item_publick_all_4,
            cls.item_publick_all_5,
            cls.item_publick_all_6,
            cls.item_unpublick_7,
            cls.item_unpublick_8,
            cls.item_unpublick_9,
        ]:
            item.full_clean()
            item.save()
            if item.pk < 5:
                item.tags.set(
                    [cls.tag_publick_ussr, cls.tag_unpublick_wildeberries],
                )
            else:
                item.tags.set(
                    [cls.tag_publick_vegetables, cls.tag_unpublick_apple],
                )

    @classmethod
    def tearDown(cls):
        catalog.models.Tag.objects.all().delete()
        catalog.models.Category.objects.all().delete()
        catalog.models.Item.objects.all().delete()
        return super().tearDown(cls)

    def check_query_fields(self, queryset):
        for item in queryset:
            for correct_field in [
                "id",
                "name",
                "text",
                "category_id",
                "_prefetched_objects_cache",
            ]:
                self.assertIn(correct_field, item.__dict__)
            for uncorrect_field in [
                "is_published",
                "is_on_main",
                "image",
                "main_image",
            ]:
                self.assertNotIn(uncorrect_field, item.__dict__)
            for tag in item.tags.all():
                self.assertIn("name", tag.__dict__)
                self.assertNotIn("is_published", tag.__dict__)
            self.assertNotIn("is_published", item.category.__dict__)

    def test_item_homepage_publick(self):
        response = Client().get(reverse("homepage:main"))
        count_items = catalog.models.Item.objects.on_main().count()
        self.assertEqual(len(response.context["items"]), count_items)
        self.check_query_fields(response.context["items"])

    def test_item_catalog_publick(self):
        response = Client().get("/catalog/")
        count_items = catalog.models.Item.objects.published().count()
        self.assertEqual(len(response.context["items"]), count_items)
        self.check_query_fields(response.context["items"])

    def test_instance_context(self):
        response = Client().get(
            reverse(
                "catalog:item_detail",
                args=[self.item_publick_catalog_1.pk],
            ),
        )
        self.assertIsInstance(response.context["item"], catalog.models.Item)

    def test_item_notfound(self):
        response = Client().get(
            reverse(
                "catalog:item_detail",
                args=[self.item_unpublick_8.pk],
            ),
        )
        self.assertNotIn("item", response.context)
