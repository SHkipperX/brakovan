from django.core import exceptions
from django.test import TestCase


import catalog.models

__all__ = []


class TagCategoryTest(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.tag = catalog.models.Tag.objects.create(
            name="nonunique",
            slug="nonunique",
        )
        cls.category = catalog.models.Category.objects.create(
            name="nonunique",
            slug="nonunique",
        )

    def tearDown(self) -> None:
        catalog.models.Tag.objects.all().delete()
        catalog.models.Category.objects.all().delete()
        return super().tearDown()

    def test_tag_fail(self):
        self.tag_count = catalog.models.Tag.objects.count()

        with self.assertRaises(exceptions.ValidationError):
            self.test_tag = catalog.models.Tag(
                name="nonunique",
                slug="test_slug",
            )
            self.test_tag.full_clean()
            self.test_tag.save()

        self.assertEqual(catalog.models.Tag.objects.count(), self.tag_count)

    def test_tag_char_fail(self):
        self.tag_count = catalog.models.Tag.objects.count()

        with self.assertRaises(exceptions.ValidationError):
            self.test_tag = catalog.models.Tag(
                name="/?11n#onu*niqu'~e",
                slug="test_slug",
            )
            self.test_tag.full_clean()
            self.test_tag.save()

        self.assertEqual(catalog.models.Tag.objects.count(), self.tag_count)

    def test_tag_char2_fail(self):
        self.tag_count = catalog.models.Tag.objects.count()

        with self.assertRaises(exceptions.ValidationError):
            self.test_tag = catalog.models.Tag(
                name="//nonu_niqu%e",
                slug="test_slug",
            )
            self.test_tag.full_clean()
            self.test_tag.save()

        self.assertEqual(catalog.models.Tag.objects.count(), self.tag_count)

    def test_category_fail(self):
        self.category_count = catalog.models.Category.objects.count()

        with self.assertRaises(exceptions.ValidationError):
            self.test_category = catalog.models.Category(
                name="nonunique",
                slug="test_slug",
            )
            self.test_category.full_clean()
            self.test_category.save()

        self.assertEqual(
            catalog.models.Category.objects.count(),
            self.category_count,
        )

    def test_category_char_fail(self):
        self.category_count = catalog.models.Category.objects.count()

        with self.assertRaises(exceptions.ValidationError):
            self.test_category = catalog.models.Category(
                name="/?11n#onu*niqu'~e",
                slug="test_slug",
            )
            self.test_category.full_clean()
            self.test_category.save()

        self.assertEqual(
            catalog.models.Category.objects.count(),
            self.category_count,
        )

    def test_category_char2_fail(self):
        self.category_count = catalog.models.Category.objects.count()

        with self.assertRaises(exceptions.ValidationError):
            self.test_category = catalog.models.Category(
                name="//НonU_niqu%e",
                slug="test_slug",
            )
            self.test_category.full_clean()
            self.test_category.save()

        self.assertEqual(
            catalog.models.Category.objects.count(),
            self.category_count,
        )
