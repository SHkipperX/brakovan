from django.test import Client, TestCase
from django.urls import reverse


__all__ = []


class TestDateEndpoint(TestCase):
    fixtures = ("fixtures/date_data.json",)

    def test_new_endpoint(self):
        response = Client().get(reverse("catalog:new"))
        self.assertEqual(len(response.context["items"]), 8)

    def test_friday_endpoint(self):
        response = Client().get(reverse("catalog:friday"))
        self.assertEqual(len(response.context["items"]), 2)

    def test_unverified_endpoint(self):
        response = Client().get(reverse("catalog:unverified"))
        self.assertEqual(len(response.context["items"]), 7)
