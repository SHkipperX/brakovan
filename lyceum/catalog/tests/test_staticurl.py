from http import HTTPStatus

from django.test import Client, TestCase
from django.urls import reverse


__all__ = []


class StaticUrlTest(TestCase):
    def test_catalog_ok(self):
        response = Client().get("/catalog/")
        self.assertContains(
            response,
            "Список товаров",
            status_code=HTTPStatus.OK,
        )

    def test_reverse(self):
        reversed_url = reverse("catalog:item_detail", args=[1])
        expected_url = "/catalog/1/"

        self.assertEqual(reversed_url, expected_url)
