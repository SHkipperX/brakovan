from django.contrib import admin

import catalog.models


__all__ = []


class ImageInline(admin.TabularInline):
    model = catalog.models.ItemMainImage
    readonly_fields = (
        "id",
        catalog.models.ItemMainImage.image_tmb,
    )
    extra = 1


class ImagesInline(admin.TabularInline):
    model = catalog.models.ItemImages
    readonly_fields = (
        "id",
        catalog.models.ItemImages.image_tmb,
    )


@admin.register(catalog.models.Item)
class ItemAdmin(admin.ModelAdmin):
    inlines = (
        ImageInline,
        ImagesInline,
    )
    fields = (
        catalog.models.Item.name.field.name,
        catalog.models.Item.is_published.field.name,
        catalog.models.Item.text.field.name,
        catalog.models.Item.tags.field.name,
        catalog.models.Item.category.field.name,
        catalog.models.Item.is_on_main.field.name,
    )

    filter_horizontal = [catalog.models.Item.tags.field.name]
    list_display = (
        catalog.models.Item.name.field.name,
        catalog.models.Item.is_published.field.name,
    )

    list_editable = (catalog.models.Item.is_published.field.name,)
    list_display_links = (catalog.models.Item.name.field.name,)


@admin.register(catalog.models.Tag)
class TagAdmin(admin.ModelAdmin):
    fields = (
        catalog.models.Tag.name.field.name,
        catalog.models.Tag.is_published.field.name,
        catalog.models.Tag.slug.field.name,
    )
    list_display = (
        "id",
        catalog.models.Tag.name.field.name,
        catalog.models.Tag.is_published.field.name,
        catalog.models.Tag.slug.field.name,
    )

    list_editable = (
        catalog.models.Tag.is_published.field.name,
        catalog.models.Tag.name.field.name,
    )

    list_display_links = ("id",)


@admin.register(catalog.models.Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = (
        catalog.models.Category.name.field.name,
        catalog.models.Category.is_published.field.name,
        catalog.models.Category.slug.field.name,
        catalog.models.Category.weight.field.name,
    )
    list_display = (
        "id",
        catalog.models.Category.name.field.name,
        catalog.models.Category.is_published.field.name,
        catalog.models.Category.slug.field.name,
        catalog.models.Category.weight.field.name,
    )

    list_editable = (
        catalog.models.Category.is_published.field.name,
        catalog.models.Category.name.field.name,
        catalog.models.Category.slug.field.name,
    )

    list_display_links = ("id",)
