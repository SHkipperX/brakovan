from django.core import exceptions, validators
from django.utils.deconstruct import deconstructible

__all__ = []


@deconstructible
class ValidateMustContain(validators.BaseValidator):
    def __init__(self, *args):
        self.args = args

    def __call__(self, value):
        if True not in (True for i in self.args if i in value.lower()):
            raise exceptions.ValidationError(
                ("%(value)s is not in %(arg)s"),
                params={"value": value, "arg": self.args},
            )

    def __eq__(self, __value: object):
        return super().__eq__(__value)
