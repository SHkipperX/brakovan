from django.shortcuts import get_list_or_404, get_object_or_404, render
from django.urls import reverse

from catalog.models import Item, ItemImages, ItemMainImage

__all__ = []

PATH = "catalog/"


def item_list(request):  # /catalog/
    templates = PATH + "item_list.html"
    rockets = Item.objects.published()
    url = reverse("catalog:item_list")
    context = {
        "url": url,
        "items": rockets,
        "title_page": "Наши товары",
    }
    return render(request, templates, context)


def item_detail(request, num):  # /catalog/<int:num>/
    templates = PATH + "item_detail.html"
    url = reverse("catalog:item_detail", args=[num])
    context = {
        "url": url,
    }

    rocket = get_object_or_404(Item.objects.item_detail(num))
    images = ItemImages.objects.item_images(rocket)
    main_image = ItemMainImage.objects.main_image(rocket)
    context["item"] = rocket
    context["images"] = images
    context["main_image"] = main_image
    context["title_page"] = rocket.name
    return render(request, templates, context)


def new_item(request):
    templates = PATH + "event_item.html"
    context = {}
    url = reverse("catalog:new")
    items = get_list_or_404(Item.objects.new())
    context["url"] = url
    context["items"] = items
    context["title_page"] = "Новинки"
    return render(request, templates, context)


def friday_item(request):
    templates = PATH + "event_item.html"
    context = {}
    url = reverse("catalog:friday")
    items = get_list_or_404(Item.objects.friday())
    context["url"] = url
    context["items"] = items
    context["title_page"] = "Пятница"
    return render(request, templates, context)


def unverified_item(request):
    templates = PATH + "event_item.html"
    context = {}
    url = reverse("catalog:unverified")

    items = get_list_or_404(Item.objects.unverified())
    context["url"] = url
    context["items"] = items
    context["title_page"] = "Непроверенное"
    return render(request, templates, context)
