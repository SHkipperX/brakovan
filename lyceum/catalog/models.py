import re

from ckeditor.fields import RichTextField
from django.core import exceptions, validators
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe
from sorl.thumbnail import get_thumbnail
from transliterate import translit
from transliterate.exceptions import LanguageDetectionError

from catalog.managers import ImagesManager, ItemMainImageManager
from catalog.validators import ValidateMustContain
from core.models import AbsNamePublishesed


__all__ = []


REGEX = re.compile(r"[^\w]")


class Normalize:
    @staticmethod
    def normalize(name):
        try:
            translitirated = translit(
                name.lower(),
                reversed=True,
            )
        except LanguageDetectionError:
            translitirated = name.lower()
        return REGEX.sub("", translitirated)


class ItemManager(models.Manager):
    def published(self):
        return (
            self.get_queryset()
            .select_related("category")
            .prefetch_related(
                models.Prefetch(
                    "tags",
                    queryset=Tag.objects.filter(
                        is_published=True,
                    ).only("name"),
                ),
            )
            .filter(
                is_published=True,
                category__is_published=True,
                tags__is_published=True,
            )
            .only(
                "name",
                "text",
                "category__name",
                "tags__name",
            )
        )

    def on_main(self):
        return (
            self.get_queryset()
            .filter(is_published=True, is_on_main=True)
            .select_related("category")
            .prefetch_related(
                models.Prefetch(
                    "tags",
                    queryset=Tag.objects.filter(
                        is_published=True,
                    ).only("name"),
                ),
            )
            .filter(
                is_published=True,
                category__is_published=True,
                tags__is_published=True,
            )
            .only(
                "name",
                "text",
                "category__name",
                "tags__name",
            )
            .order_by("-name")
        )

    def item_detail(self, pk):
        return (
            self.get_queryset()
            .filter(is_published=True, id=pk)
            .select_related("category")
            .prefetch_related(
                models.Prefetch(
                    "tags",
                    queryset=Tag.objects.filter(
                        is_published=True,
                    )
                    .only(
                        "name",
                    )
                    .filter(is_published=True),
                ),
            )
            .filter(is_published=True, category__is_published=True)
            .only(
                "name",
                "text",
                "category__name",
                "tags__name",
            )
        )

    def new(self):
        filter_date = timezone.datetime.now() - timezone.timedelta(weeks=1)
        return (
            self.get_queryset()
            .filter(date_created__gte=filter_date)
            .select_related("category")
            .prefetch_related(
                models.Prefetch(
                    "tags",
                    queryset=Tag.objects.filter(
                        is_published=True,
                    )
                    .only(
                        "name",
                    )
                    .filter(is_published=True),
                ),
            )
            .filter(
                is_published=True,
                category__is_published=True,
            )
            .only(
                "name",
                "text",
                "category__name",
                "tags__name",
            )
            .order_by("?")
        )

    def friday(self):
        return (
            self.get_queryset()
            .filter(date_updated__iso_week_day=5)
            .select_related("category")
            .prefetch_related(
                models.Prefetch(
                    "tags",
                    queryset=Tag.objects.filter(
                        is_published=True,
                    )
                    .only(
                        "name",
                    )
                    .filter(is_published=True),
                ),
            )
            .filter(
                is_published=True,
                category__is_published=True,
            )
            .only(
                "name",
                "text",
                "category__name",
                "tags__name",
            )
        )

    def unverified(self):
        met = timezone.timedelta(seconds=0.5)
        return (
            self.get_queryset()
            .select_related("category")
            .prefetch_related(
                models.Prefetch(
                    "tags",
                    queryset=Tag.objects.filter(
                        is_published=True,
                    )
                    .only(
                        "name",
                    )
                    .filter(is_published=True),
                ),
            )
            .filter(
                is_published=True,
                date_created__gte=models.F("date_updated") - met,
                date_created__lte=models.F("date_updated") + met,
                category__is_published=True,
            )
            .only(
                "name",
                "text",
                "category__name",
                "tags__name",
            )
        )


class Category(AbsNamePublishesed):
    class Meta:
        verbose_name = "категория"
        verbose_name_plural = "категории"

    slug = models.SlugField(
        verbose_name="слаг",
        max_length=200,
        unique=True,
        validators=[validators.MaxLengthValidator(200)],
    )
    weight = models.SmallIntegerField(
        verbose_name="Вес",
        default=100,
        validators=[
            validators.MinValueValidator(0),
            validators.MaxValueValidator(32767),
        ],
    )

    normalized_words = models.CharField(
        verbose_name="Нормализованные слова",
        max_length=150,
    )

    def save(self, *args, **kwargs):
        self.normalized_words = Normalize.normalize(self.name)
        self.name = self.name.lower()
        super().save(*args, **kwargs)

    def clean(self):
        name = Normalize.normalize(self.name)
        if Category.objects.filter(
            normalized_words=name,
            is_published=self.is_published,
        ).first():
            raise exceptions.ValidationError(
                "Категория с таким/похожим именем уже существует!",
            )
        self.normalized_words = name
        return super().clean()

    def __str__(self) -> str:
        return self.name


class Tag(AbsNamePublishesed):
    class Meta:
        verbose_name = "тег"
        verbose_name_plural = "теги"

    slug = models.SlugField(
        verbose_name="слаг",
        max_length=200,
        unique=True,
        validators=[
            validators.MaxLengthValidator(200),
        ],
    )

    normalized_words = models.CharField(
        verbose_name="Нормализованные слова",
        max_length=150,
    )

    def save(self, *args, **kwargs):
        self.normalized_words = Normalize.normalize(self.name)
        self.name = self.name.lower()
        super().save(*args, **kwargs)

    def clean(self):
        name = Normalize.normalize(self.name)

        if Tag.objects.filter(
            normalized_words=name,
            is_published=self.is_published,
        ).first():
            raise exceptions.ValidationError(
                "Категория с таким/похожим именем уже существует!",
            )
        self.normalized_words = name
        return super().clean()

    def __str__(self) -> str:
        return self.name[:15]


class ItemMainImage(models.Model):
    objects = ItemMainImageManager()
    item = models.ForeignKey(
        "Item",
        on_delete=models.CASCADE,
        verbose_name="товар",
        related_name="main_image",
    )

    image = models.ImageField(
        upload_to="catalog/",
        verbose_name="изображение",
    )

    @property
    def get_300x300px(self):
        return get_thumbnail(self.image, "300x300", crop="center", quality=51)

    def image_tmb(self):
        if self.image:
            return mark_safe(
                f"<img src=/media/{self.get_300x300px}"
                " "
                "class='card-img-top'",
            )
        return "Изображение не найдено/отсутсвует"

    image_tmb.short_description = "превью"
    image_tmb.allow_tags = True

    fields = ["image_tmb"]

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = "изображение"
        verbose_name_plural = "изображения"


class ItemImages(models.Model):
    objects = ImagesManager()

    item = models.ForeignKey(
        "Item",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name="товар",
        related_query_name="item_images",
    )

    image = models.ImageField(
        upload_to="catalog/",
        null=True,
        blank=True,
        verbose_name="изображение",
    )

    @property
    def get_300x300px(self):
        return get_thumbnail(
            self.image,
            "300x300",
            crop="center",
            quality=51,
        )

    def image_tmb(self):
        if self.image:
            return mark_safe(
                f"<img src=/media/{self.get_300x300px}"
                " "
                "class='card-img-top'",
            )
        return "Изображение не найдено/отсутсвует"

    image_tmb.short_description = "превью"
    image_tmb.allow_tags = True

    fields = ["image_tmb"]

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = "изображения"
        verbose_name_plural = "изображения"


class Item(AbsNamePublishesed):
    objects = ItemManager()

    class Meta:
        ordering = (
            "category__name",
            "-name",
        )
        verbose_name = "товар"
        verbose_name_plural = "товары"

    is_on_main = models.BooleanField(
        verbose_name="публикация на главной",
        default=False,
    )

    text = RichTextField(
        verbose_name="Текст",
        validators=[ValidateMustContain("превосходно", "роскошно")],
    )

    tags = models.ManyToManyField(
        Tag,
        verbose_name="Теги",
        related_name="tags",
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        verbose_name="Категории",
        related_query_name="category",
    )

    date_created = models.DateTimeField(
        verbose_name="дата создания товара",
        auto_now_add=True,
    )
    date_updated = models.DateTimeField(
        verbose_name="дата обновления товара",
        auto_now=True,
    )

    def __str__(self) -> str:
        return self.name[:15]
