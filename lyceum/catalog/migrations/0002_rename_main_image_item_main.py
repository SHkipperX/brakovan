# Generated by Django 4.2.5 on 2023-10-31 17:00

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("catalog", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="item",
            old_name="main_image",
            new_name="main",
        ),
    ]
