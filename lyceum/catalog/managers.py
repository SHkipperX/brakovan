from django.db import models

__all__ = []


class ImagesManager(models.Manager):
    def item_images(self, item):
        return (
            self.get_queryset()
            .select_related(
                "item",
            )
            .filter(
                item_id=item.pk,
            )
        )


class ItemMainImageManager(models.Manager):
    def main_image(self, item):
        return (
            super()
            .get_queryset()
            .select_related("item")
            .filter(item_id=item.pk)
            .first()
        )
