from django.urls import path, register_converter

from catalog import converters, views

register_converter(converters.NumConverter, "fnn")

app_name = "catalog"


urlpatterns = [
    path("", views.item_list, name="item_list"),
    path("<int:num>/", views.item_detail, name="item_detail"),
    path("new/", views.new_item, name="new"),
    path("friday/", views.friday_item, name="friday"),
    path("unverified/", views.unverified_item, name="unverified"),
]
