from http import HTTPStatus

from django.test import Client, TestCase
from django.urls import reverse

__all__ = []


class StaticUrlTest(TestCase):
    def test_homepage_endpoint(self):
        response = Client().get(reverse("homepage:main"))
        self.assertContains(response, "На главную", status_code=HTTPStatus.OK)

    def test_teapot(self):
        response = Client().get(reverse("homepage:coffee"))
        self.assertContains(
            response,
            "Я чайник",
            status_code=HTTPStatus.IM_A_TEAPOT,
        )
