from http import HTTPStatus

from django.test import Client, TestCase
from django.urls import reverse
from forms import EchoForm

__all__ = []


class StaticUrlTest(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.form = EchoForm()
        super().setUpClass()

    def test_echo_submit_success(self):
        not_reverse = (
            "Привет, этo почтi-почти Pуcский текст@, просто≈ Как-то со спецü "
            "символами:) ¡сорри∑! Hу ещё раз ¡сорри! Ёжика не видели?"
        )
        reverse_word = (
            "тевирП, этo почтi-итчоп Pуcский тскет@, отсорп≈ каК-от ос спецü "
            "ималовмис:) ¡иррос∑! Hу ёще зар ¡иррос! акижЁ ен иледив?"
        )

        data = {
            "text": not_reverse,
        }

        response = {
            Client()
            .post(reverse("homepage:echo_submit"), data=data)
            .content.decode()
            for _ in range(10)
        }

        self.assertIn(not_reverse, response)
        self.assertIn(reverse_word, response)

    def test_echo_submit_405(self):
        response = Client().get(reverse("homepage:echo_submit"))
        self.assertEquals(HTTPStatus.METHOD_NOT_ALLOWED, response.status_code)
