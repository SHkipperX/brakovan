from http import HTTPStatus

from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.http import require_GET, require_POST

from catalog.models import Item
from homepage.forms import EchoForm

__all__ = []

PATH = "homepage/"


def main(request):
    template = PATH + "main.html"
    url = reverse("homepage:main")
    context = {"url": url}
    rockets = Item.objects.on_main()
    context["items"] = rockets
    context["title_page"] = "Главная"
    return render(request, template, context)


def coffee(request):
    if request.user.id:
        request.user.profile.coffee_count += 1
        request.user.profile.save()
    return HttpResponse(content="Я чайник", status=HTTPStatus.IM_A_TEAPOT)


@require_GET
def echo(request):
    template = "feedback/feedback.html"
    context = {}
    context["form"] = EchoForm()
    return render(request, template, context)


@require_POST
def echo_submit(request):
    form = EchoForm(request.POST or None)
    if form.is_valid():
        form = form.cleaned_data
        return HttpResponse(
            content=form.get("text"),
            content_type="text/plain;charset=utf-8",
            charset="utf-8",
            headers={"Accept-Charset": "utf-8"},
        )
    return HttpResponseBadRequest(
        "Неверный формат формы",
    )
