[![pipeline](https://gitlab.com/SHkipperX/brakovan/badges/main/pipeline.svg)](https://gitlab.com/SHkipperX/brakovan/commits/main)

Каждый пункт предполагает, что вы находитесь не в корне проекта!
>0. Как выгялдит диаграмма базы данных монжно посмотреть тут
![ER_items.jpg](ER_items.jpg)
![ER_feedback.jpg](ER_feedback.jpg)
>1. Установка виртуального окружения
```
python -m venv venv
```
>2. Активируем виртуальное окружение
```
source venv/bin/activate
```
>3. Обновим __pip__
```
python -m pip install --upgrade pip
```
>4. Установим необходимые пакеты для запуска проекта
```
pip install -r requirements/prod.txt -r requirements/test.txt
```
>5. этот файл ".env" должен быть у тебя самого, в котором будут
критические данные проекта: DEBUG, SECRET_KEY, LOCAL_HOST
```
mv lyceum/template.env .env
```
>6. Локализация сайта
```
cd lyceum
django-admin makemessages -l en
django-admin makemessages -l ru
django-admin compilemessages
```
>7. И наконец запустим проект
```
cd lyceum
python manage.py makemigrations
python manage.py migrate
python manage.py sqlmigrate catalog 0001
python -Xutf8 manage.py loaddata fixtures/data.json
python manage.py runserver
```